# FriendlyColors

The FriendlyColors function takes RGB values for 2 predefined color "classes", and numerically derives the rest of a color family that has the same relationship between color components as the input colors.

In terms of this function, a color family is defined as having an RBG value for each of the color "classes" in the following list:

    - black         [0.0 R, 0.0 G, 0.0 B]
    - grey          [0.5 R, 0.5 G, 0.5 B]
    - white         [1.0 R, 1.0 G, 1.0 B]
    - red           [1.0 R, 0.0 G, 0.0 B]
    - orange        [1.0 R, 0.5 G, 0.0 B]
    - yellow        [1.0 R, 1.0 G, 0.0 B]
    - chartreuse    [0.5 R, 1.0 G, 0.0 B]
    - green         [0.0 R, 1.0 G, 0.0 B]
    - spring green  [0.0 R, 1.0 G, 0.5 B]
    - cyan          [0.0 R, 1.0 G, 1.0 B]
    - azure         [0.0 R, 0.5 G, 1.0 B]
    - blue          [0.0 R, 0.0 G, 1.0 B]
    - indigo        [0.5 R, 0.0 G, 1.0 B]
    - magenta       [1.0 R, 0.0 G, 1.0 B]
    - rose          [1.0 R, 0.0 G, 0.5 B]

The 2 input color "classes" that are provided must be chosen such that between the 2 of them, each color component (R, G, B) has two different data points according to the above list. For example, the color "classes" cyan and orange are a valid input combination, as they provide two different data points of Red (cyan: 0.0, orange: 1.0), Green (cyan: 1.0, orange: 0.5), and Blue (cyan: 1.0, orange: 0.0).

Given a valid input combination, the 0-100% ranges of each RGB component are redefined to a different scale, based on the RGB values of the inputs. The above list of colors is then regenerated using the new values for 0.0, 0.5, and 1.0.

Example:

    fc = FriendlyColors('cyan', [0.1, 0.9, 0.7], 'orange', [0.85 0.45, 0.1]);

Result:

![Result](example.png)
