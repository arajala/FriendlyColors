function colors = FriendlyColors(color1Name, color1RGB, color2Name, color2RGB)

    MIN = 1;
    HALF = 2;
    MAX = 3;

    redRange = [-1, -1, -1];
    greenRange = [-1, -1, -1];
    blueRange = [-1, -1, -1];

    [redRange, greenRange, blueRange] = ...
        ParseInputColor(color1Name, color1RGB, redRange, greenRange, blueRange);

    [redRange, greenRange, blueRange] = ...
        ParseInputColor(color2Name, color2RGB, redRange, greenRange, blueRange);

    if length(find(redRange == -1)) > 1
        error('Input colors did not provide 2 different red values')
    elseif length(find(greenRange == -1)) > 1
        error('Input colors did not provide 2 different green values')
    elseif length(find(blueRange == -1)) > 1
        error('Input colors did not provide 2 different blue values')
    end

    redRange = CompleteRange(redRange);
    greenRange = CompleteRange(greenRange);
    blueRange = CompleteRange(blueRange);

    colors.black        = [redRange(MIN),   greenRange(MIN),    blueRange(MIN)];
    colors.grey         = [redRange(HALF),  greenRange(HALF),   blueRange(HALF)];
    colors.white        = [redRange(MAX),   greenRange(MAX),    blueRange(MAX)];
    colors.red          = [redRange(MAX),   greenRange(MIN),    blueRange(MIN)];
    colors.orange       = [redRange(MAX),   greenRange(HALF),   blueRange(MIN)];
    colors.yellow       = [redRange(MAX),   greenRange(MAX),    blueRange(MIN)];
    colors.chartreuse   = [redRange(HALF),  greenRange(MAX),    blueRange(MIN)];
    colors.green        = [redRange(MIN),   greenRange(MAX),    blueRange(MIN)];
    colors.springgreen  = [redRange(MIN),   greenRange(MAX),    blueRange(HALF)];
    colors.cyan         = [redRange(MIN),   greenRange(MAX),    blueRange(MAX)];
    colors.azure        = [redRange(MIN),   greenRange(HALF),   blueRange(MAX)];
    colors.blue         = [redRange(MIN),   greenRange(MIN),    blueRange(MAX)];
    colors.indigo       = [redRange(MIN),   greenRange(HALF),   blueRange(MAX)];
    colors.magenta      = [redRange(MAX),   greenRange(MIN),    blueRange(MAX)];
    colors.rose         = [redRange(MAX),   greenRange(MIN),    blueRange(HALF)];

    figure

    subplot(4,4,1)
    title('black')
    set(gca, 'Color', colors.black)

    subplot(4,4,2)
    title('grey')
    set(gca, 'Color', colors.grey)

    subplot(4,4,3)
    title('white')
    set(gca, 'Color', colors.white)

    subplot(4,4,4)
    title('red')
    set(gca, 'Color', colors.red)

    subplot(4,4,5)
    title('orange')
    set(gca, 'Color', colors.orange)

    subplot(4,4,6)
    title('yellow')
    set(gca, 'Color', colors.yellow)

    subplot(4,4,7)
    title('chartreuse')
    set(gca, 'Color', colors.chartreuse)

    subplot(4,4,8)
    title('green')
    set(gca, 'Color', colors.green)

    subplot(4,4,9)
    title('spring green')
    set(gca, 'Color', colors.springgreen)

    subplot(4,4,10)
    title('cyan')
    set(gca, 'Color', colors.cyan)

    subplot(4,4,11)
    title('azure')
    set(gca, 'Color', colors.azure)

    subplot(4,4,12)
    title('blue')
    set(gca, 'Color', colors.blue)

    subplot(4,4,13)
    title('indigo')
    set(gca, 'Color', colors.indigo)

    subplot(4,4,14)
    title('magenta')
    set(gca, 'Color', colors.magenta)

    subplot(4,4,15)
    title('rose')
    set(gca, 'Color', colors.rose)

end

function [redRange, greenRange, blueRange] = ParseInputColor(colorName, colorRGB, redRange, greenRange, blueRange)

    RED = 1;
    GREEN = 2;
    BLUE = 3;
    
    MIN = 1;
    HALF = 2;
    MAX = 3;

    switch colorName
    case 'black'
        redRange(MIN)       = colorRGB(RED);
        greenRange(MIN)     = colorRGB(GREEN);
    blueRange(MIN)          = colorRGB(BLUE);
    case 'grey'
        redRange(HALF)      = colorRGB(RED);
        greenRange(HALF)    = colorRGB(GREEN);
        blueRange(HALF)     = colorRGB(BLUE);
    case 'white'
        redRange(MIN)       = colorRGB(RED);
        greenRange(MIN)     = colorRGB(GREEN);
        blueRange(MIN)      = colorRGB(BLUE);
    case 'red'
        redRange(MAX)       = colorRGB(RED);
        greenRange(MIN)     = colorRGB(GREEN);
        blueRange(MIN)      = colorRGB(BLUE);
    case 'orange'
        redRange(MAX)       = colorRGB(RED);
        greenRange(HALF)    = colorRGB(GREEN);
        blueRange(MIN)      = colorRGB(BLUE);
    case 'yellow'
        redRange(MAX)       = colorRGB(RED);
        greenRange(MAX)     = colorRGB(GREEN);
        blueRange(MIN)      = colorRGB(BLUE);
    case 'chartreuse'
        redRange(HALF)      = colorRGB(RED);
        greenRange(MAX)     = colorRGB(GREEN);
        blueRange(MIN)      = colorRGB(BLUE);
    case 'green'
        redRange(MIN)       = colorRGB(RED);
        greenRange(MAX)     = colorRGB(GREEN);
        blueRange(MIN)      = colorRGB(BLUE);
    case 'spring green'
        redRange(MIN)       = colorRGB(RED);
        greenRange(MAX)     = colorRGB(GREEN);
        blueRange(HALF)     = colorRGB(BLUE);
    case 'cyan'
        redRange(MIN)       = colorRGB(RED);
        greenRange(MAX)     = colorRGB(GREEN);
        blueRange(MAX)      = colorRGB(BLUE);
    case 'azure'
        redRange(MIN)       = colorRGB(RED);
        greenRange(HALF)    = colorRGB(GREEN);
        blueRange(MAX)      = colorRGB(BLUE);
    case 'blue'
        redRange(MIN)       = colorRGB(RED);
        greenRange(MIN)     = colorRGB(GREEN);
        blueRange(MAX)      = colorRGB(BLUE);
    case 'indigo'
        redRange(HALF)      = colorRGB(RED);
        greenRange(MIN)     = colorRGB(GREEN);
        blueRange(MAX)      = colorRGB(BLUE);
    case 'magenta'
        redRange(MAX)       = colorRGB(RED);
        greenRange(MIN)     = colorRGB(GREEN);
        blueRange(HALF)     = colorRGB(BLUE);
    case 'rose'
        redRange(MAX)       = colorRGB(RED);
        greenRange(MIN)     = colorRGB(GREEN);
        blueRange(HALF)     = colorRGB(BLUE);
    end

end

function range = CompleteRange(range)

    if range(1) == -1
        range(1) = range(2) - (range(3) - range(2));
    elseif range(2) == -1
        range(2) = (range(1) + range(3)) / 2.0;
    elseif range(3) == -1
        range(3) = range(2) + (range(2) - range(1));
    end

end
